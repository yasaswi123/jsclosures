function counterFactory(){
    let counter = 7
    //error handling
    if(isNaN(Number(counter))){
        counter=0
    }

    function increment(){
        ++counter
        return counter
    }
    function decrement(){
        counter--
        return counter
    }
    return {increment,decrement}
}

module.exports = counterFactory
