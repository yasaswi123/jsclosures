function limitFunctionCallCount(cb, n) {
    //error handling
    if(typeof cb !== 'function' || typeof n != 'number'){
        return "Either cb is not a function or n is not a number"
    }

    let index=0
    return function(){
        if(index<n){
            index++;
            return cb();
        }
        else{
            return null;
        }
    }
}

module.exports=limitFunctionCallCount