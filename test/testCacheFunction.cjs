const cacheFunction = require('../cacheFunction.cjs')

function cb(args){
    const addElements = args.reduce((accumulator,currentvalue)=>accumulator+currentvalue,0)
    return addElements
}

const cacheAdd = cacheFunction(cb);

console.log(cacheAdd([8,9,5])); 
console.log(cacheAdd([1,2])); 
console.log(cacheAdd([8,9,5])); 
console.log(cacheAdd([4,5])); 

