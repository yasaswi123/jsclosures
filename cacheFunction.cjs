function cacheFunction(cb) {
    //error handling
    if (typeof cb !== 'function' ) {
        return 'cb is not a function'
    }

    const cache = {};
    return function(...args) {
        if (args.length == 0) {
            return 'Array is missing';
        }
        const key = JSON.stringify(args);
        if (!(key in cache) ){
            cache[key] = cb(...args);
        }
        return cache[key];
    }
}

module.exports = cacheFunction

